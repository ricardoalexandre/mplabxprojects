 
#ifndef TEMPERATUREREADING_H
#define	TEMPERATUREREADING_H

#define filterADC  5
#define mimTemperature 180
#define maxTemperature 450

int readIronTemperature ();

void convertIronTemperature();

#endif	/* XC_HEADER_TEMPLATE_H */

